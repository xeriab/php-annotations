<center>![Annotations](.gitlab/annotations-logo.png "Annotations")</center>

Annotations
===========

PHP annotations library.

Resources
---------

  * [Documentation](https://xeriab.gitlab.com/docs/php-annotations/index.html)
  * [Contributing](https://xeriab.gitlab.com/docs/contributing/index.html)
  * [Changes log](CHANGES.md).
  * [Report issues](https://gitlab.com/xeriab/php-annotations/issues) and
    [Send Pull/Merge Requests](https://gitlab.com/xeriab/php-annotations/merge_requests)
    in the [main annotations repository](https://gitlab.com/xeriab/php-annotations)

## Copyright and License

Copyright [Xeriab Nabil](#). under the [MIT license](LICENSE).
