<?php

namespace Xeriab\Annotations;

use Xeriab\Support\Lexer\AbstractLexer;

/**
 * Simple lexer for docblock annotations.
 */
final class DocLexer extends AbstractLexer
{
    const T_NONE                = 1;
    const T_INTEGER             = 2;
    const T_STRING              = 3;
    const T_FLOAT               = 4;

    // All tokens that are also identifiers should be >= 100
    const T_IDENTIFIER          = 100;
    const T_AT                  = 101;
    const T_CLOSE_CURLY_BRACES  = 102;
    const T_CLOSE_PARENTHESIS   = 103;
    const T_COMMA               = 104;
    const T_EQUALS              = 105;
    const T_FALSE               = 106;
    const T_NAMESPACE_SEPARATOR = 107;
    const T_OPEN_CURLY_BRACES   = 108;
    const T_OPEN_PARENTHESIS    = 109;
    const T_TRUE                = 110;
    const T_NULL                = 111;
    const T_COLON               = 112;
    const T_SEMI_COLON          = 113;
    const T_OPEN_BRACKET        = 114;
    const T_CLOSE_BRACKET       = 115;

    const T_SINGLE_QUOTATION    = 116;
    const T_DOUBLE_QUOTATION    = 117;

    /**
     *
     * @var array
     */
    protected $noCase = [
        '@'  => self::T_AT,
        ','  => self::T_COMMA,
        '\'' => self::T_SINGLE_QUOTATION,
        '"'  => self::T_DOUBLE_QUOTATION,
        '('  => self::T_OPEN_PARENTHESIS,
        ')'  => self::T_CLOSE_PARENTHESIS,
        '{'  => self::T_OPEN_CURLY_BRACES,
        '}'  => self::T_CLOSE_CURLY_BRACES,
        '['  => self::T_OPEN_BRACKET,
        ']'  => self::T_CLOSE_BRACKET,
        '='  => self::T_EQUALS,
        ':'  => self::T_COLON,
        ';'  => self::T_SEMI_COLON,
        '\\' => self::T_NAMESPACE_SEPARATOR,
    ];

    /**
     *
     * @var array
     */
    protected $withCase = [
        'true'  => self::T_TRUE,
        'yes'   => self::T_TRUE,
        'on'    => self::T_TRUE,

        'TRUE'  => self::T_TRUE,
        'YES'   => self::T_TRUE,
        'ON'    => self::T_TRUE,

        'True'  => self::T_TRUE,
        'Yes'   => self::T_TRUE,
        'On'    => self::T_TRUE,

        'false' => self::T_FALSE,
        'no'    => self::T_FALSE,
        'off'   => self::T_FALSE,

        'FALSE' => self::T_FALSE,
        'NO'    => self::T_FALSE,
        'OFF'   => self::T_FALSE,

        'False' => self::T_FALSE,
        'No'    => self::T_FALSE,
        'Off'   => self::T_FALSE,

        'null'  => self::T_NULL,
        'nil'   => self::T_NULL,
        'none'  => self::T_NULL,

        'NULL'  => self::T_NULL,
        'NIL'   => self::T_NULL,
        'NONE'  => self::T_NULL,

        'Null'  => self::T_NULL,
        'Nil'   => self::T_NULL,
        'None'  => self::T_NULL,
    ];

    /**
     * Whether the next token starts immediately, or if there were
     * non-captured symbols before that
     */
    public function nextTokenIsAdjacent(): bool
    {
        return $this->token === null
            || ($this->lookahead !== null
                && ($this->lookahead['position'] - $this->token['position']) === strlen($this->token['value']));
    }

    /**
     * {@inheritdoc}
     */
    protected function getCatchablePatterns(): ?array
    {
        // return [
        //     '[a-z_\][a-z0-9_\:\]*[a-z_][a-z0-9_]*',
        //     '(?:[+-]?[0-9]+(?:[\.][0-9]+)*)(?:[eE][+-]?[0-9]+)?',
        //     '"(?:""|[^"])*+"',
        // ];

        return [
            '[a-z_\\\][a-z0-9_\:\\\]*[a-z_][a-z0-9_]*',
            '(?:[+-]?[0-9]+(?:[\.][0-9]+)*)(?:[eE][+-]?[0-9]+)?',
            '"(?:""|[^"])*+"',
        ];
    }

    /**
     * {@inheritdoc}
     */
    protected function getNonCatchablePatterns(): ?array
    {
        return ['\s+', '\*+', '(.)'];
    }

    /**
     * {@inheritdoc}
     */
    protected function getType(?string &$value = null): ?int
    {
        // $type = self::T_NONE;

        // if ($value[0] === '"') {
        //     $value = \str_replace('""', '"', \substr($value, 1, \strlen($value) - 2));

        //     return self::T_STRING;
        // }

        // if ($value[0] === '\'') {
        //     $value = \str_replace('\'\'', '\'', \substr($value, 1, \strlen($value) - 2));

        //     return self::T_STRING;
        // }

        // if (isset($this->noCase[$value])) {
        //     return $this->noCase[$value];
        // }

        // if ($value[0] === '_' || $value[0] === '\\' || \ctype_alpha($value[0])) {
        //     return self::T_IDENTIFIER;
        // }

        // $lowerValue = \strtolower($value);

        // if (isset($this->withCase[$lowerValue])) {
        //     return $this->withCase[$lowerValue];
        // }

        // // Checking numeric value
        // if (\is_numeric($value)) {
        //     return (\strpos($value, '.') !== false || \stripos($value, 'e') !== false)
        //         ? self::T_FLOAT: self::T_INTEGER;
        // }

        // return $type;

        $type = self::T_NONE;

        if ($value[0] === '"') {
            $value = \str_replace('""', '"', \substr($value, 1, \strlen($value) - 2));

            return self::T_STRING;
        }

        if (isset($this->noCase[$value])) {
            return $this->noCase[$value];
        }

        if ($value[0] === '_' || $value[0] === '\\' || \ctype_alpha($value[0])) {
            return self::T_IDENTIFIER;
        }

        $lowerValue = \strtolower($value);

        if (isset($this->withCase[$lowerValue])) {
            return $this->withCase[$lowerValue];
        }

        // Checking numeric value
        if (\is_numeric($value)) {
            return (\strpos($value, '.') !== false || \stripos($value, 'e') !== false)
                ? self::T_FLOAT : self::T_INTEGER;
        }

        return $type;
    }
}
