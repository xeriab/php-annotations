<?php

namespace Xeriab\Annotations;

use Xeriab\Annotations\Interfaces\ParserInterface;
use Xeriab\Annotations\Types\DynamicType;

/**
 * An Annotations parser
 *
 * @package Annotations
 */
class DynamicParser implements ParserInterface
{
    const TOKEN_ANNOTATION_IDENTIFIER = '@';

    const TOKEN_ANNOTATION_NAME = '[a-zA-Z\_\-\]\[][a-zA-Z0-9\_\-\.\]\[]*';

    /**
     * The lexer.
     *
     * @var \Xeriab\Annotations\DocLexer
     */
    private $lexer;

    /**
     * Current target context.
     *
     * @var integer
     */
    private $target;

    /**
     * Doc parser used to collect annotation target.
     *
     * @var \Xeriab\Annotations\DocParser
     */
    private static $metadataParser;

    /**
     * Flag to control if the current annotation is nested or not.
     *
     * @var boolean
     */
    private $isNestedAnnotation = false;

    /**
     * Hashmap containing all use-statements that are to be used when parsing
     * the given doc block.
     *
     * @var array
     */
    private $imports = [];

    /**
     * This hashmap is used internally to cache results of class_exists()
     * look-ups.
     *
     * @var array
     */
    private $classExists = [];

    /**
     * Whether annotations that have not been imported should be ignored.
     *
     * @var boolean
     */
    private $ignoreNotImportedAnnotations = false;

    private $ignoredAnnotationNamespaces = [];

    /**
     * An array of default namespaces if operating in simple mode.
     *
     * @var string[]
     */
    private $namespaces = [];

    /**
     * A list with annotations that are not causing exceptions when not resolved to an annotation class.
     *
     * The names must be the raw names as used in the class, not the fully qualified
     * class names.
     *
     * @var bool[] indexed by annotation name
     */
    private $ignoredAnnotationNames = [];

    /**
     *
     * @var string
     */
    private $context = '';

    /**
     * The regex to extract data from a single line
     *
     * @var string
     */
    protected $dataPattern;

    /**
     * Parser constructor
     */
    public function __construct()
    {
        $this->dataPattern = '/(?<=\\'. self::TOKEN_ANNOTATION_IDENTIFIER .')('
            . self::TOKEN_ANNOTATION_NAME
            .')(((?!\s\\'. self::TOKEN_ANNOTATION_IDENTIFIER .').)*)/s';
    }

    /**
     * Sets the annotation names that are ignored during the parsing process.
     *
     * The names are supposed to be the raw names as used in the class, not the
     * fully qualified class names.
     *
     * @param bool[] $names indexed by annotation name
     *
     * @return void
     */
    public function setIgnoredAnnotationNames(array $names = [])
    {
        $this->ignoredAnnotationNames = $names;
    }

    /**
     * Sets ignore on not-imported annotations.
     *
     * @param boolean $bool
     *
     * @return void
     */
    public function setIgnoreNotImportedAnnotations($bool)
    {
        $this->ignoreNotImportedAnnotations = (boolean) $bool;
    }

    /**
     * {@inheritDoc}
     */
    public function parse(string $docBlock = null, string $context = '')
    {
        $docBlock = $this->getDocblockTagsSection($docBlock);
        // D($docBlock);
        $annotations = $this->parseAnnotations($docBlock);

        foreach ($annotations as $key => $value) {
            $key = \lcfirst($key);

            if ($this->isIgnoredAnnotation($key)) {
                if (isset($annotations[$key])) {
                    unset($annotations[$key]);
                }
            }
        }

        foreach ($annotations as &$value) {
            if (1 === \count($value)) {
                $value = $value[0];
            }
        }

        return $annotations;
    }

    public static function parseAnnotation(string $value = null)
    {
    }

    private function isIgnoredAnnotation($name)
    {
        if ($this->ignoreNotImportedAnnotations
            || isset($this->ignoredAnnotationNames[$name])
        ) {
            return true;
        }

        foreach (\array_keys($this->ignoredAnnotationNamespaces) as $ignoredAnnotationNamespace) {
            $ignoredAnnotationNamespace = \rtrim($ignoredAnnotationNamespace, '\\') . '\\';

            if (0 === \stripos(\rtrim($name, '\\') . '\\', $ignoredAnnotationNamespace)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Sets the imports.
     *
     * @param array $imports
     *
     * @return void
     *
     * @throws \RuntimeException
     */
    public function setImports(array $imports)
    {
        if ($this->namespaces) {
            throw new \RuntimeException('You must either use addNamespace(), or setImports(), but not both.');
        }

        $this->imports = $imports;
    }

    /**
     * Sets current target context as bitmask.
     *
     * @param integer $target
     *
     * @return void
     */
    public function setTarget($target)
    {
        $this->target = $target;
    }

    /**
     * Filters docblock tags section, removing unwanted long and short descriptions
     *
     * @param string $docblock A docblok string without delimiters
     *
     * @return string Tag section from given docblock
     */
    protected function getDocblockTagsSection($docblock)
    {
        $docblock = $this->sanitizeDocblock($docblock);

        \preg_match(
            '/^\s*\\'.self::TOKEN_ANNOTATION_IDENTIFIER.'/m',
            $docblock,
            $matches,
            \PREG_OFFSET_CAPTURE
        );

        // Return found docblock tag section or empty string
        return isset($matches[0]) ? \substr($docblock, $matches[0][1]): '';
    }

    /**
     * Filters docblock delimiters
     *
     * @param string $docblock A raw docblok string
     *
     * @return string A docblok string without delimiters
     */
    protected function sanitizeDocblock($docblock)
    {
        return \preg_replace(
            '/\s*\*\/$|^\s*\*\s{0,1}|^\/\*{1,2}/m',
            '',
            $docblock
        );
    }

    /**
     * Creates raw [annotation => value, [...]] tree
     *
     * @param string $str
     *
     * @return array
     */
    protected function parseAnnotations($str)
    {
        $annotations = [];

        \preg_match_all($this->dataPattern, $str, $found);

        foreach ($found[2] as $key => $value) {
            $annotations[
                $this->sanitizeKey($found[1][$key])
            ][] = $this->parseValue($value, $found[1][$key]);
        }

        return $annotations;
    }

    /**
     * Parse a single annotation value
     *
     * @param string $value
     * @param string $key
     *
     * @return mixed
     */
    protected function parseValue($value, $key = null)
    {
        return (new DynamicType)->parse(trim($value), $key);
    }

    /**
     * Just a hook so derived parsers can transform annotation identifiers before they go to AST
     *
     * @param string $key
     *
     * @return string
     */
    protected function sanitizeKey($key)
    {
        return $key;
    }
}
