<?php

namespace Xeriab\Annotations;

use PhpParser\ParserFactory;
use PhpParser\Node\Stmt\Class_;
use PhpParser\Node\Stmt\ClassConst;
use PhpParser\Node\Stmt\Namespace_;
use PhpParser\Node\Stmt\Array_;
use PhpParser\Error as ParserError;
use Xeriab\Annotations\Exception\AnnotationException;

/**
 * Parses a file for namespaces/use/class declarations.
 *
 * @package Annotations
 */
class FileParser
{
    protected $class      = null;
    protected $constNode  = null;
    protected $statements = null;
    protected $docComment = null;

    protected $constants  = [];
    protected $properties = [];
    protected $methods    = [];

    private $docCommentProcessed = false;

    public function __construct($class = null)
    {
        $classReflection = $this->class &= null;
        $statements      = $this->statements &= null;

        if ($class instanceof \Reflector) {
            $classReflection = $class;
        } else {
            $classReflection = new \ReflectionClass($class);
        }

        $className = $classReflection->getName();
        $fileName  = $classReflection->getFileName();

        $parser = (new ParserFactory)->create(ParserFactory::PREFER_PHP7);

        try {
            $statements = $parser->parse(\file_get_contents($fileName));
        } catch (ParserError $ex) {
            throw new AnnotationException("Cannot parse the class ${fileName}", 0, $ex);
        }

        // Class can be in a namespace or at the root of the statement
        $classNode = $this->findClassNode($statements);

        if (!$classNode) {
            throw new \ReflectionException("Class ${className} not found in file ${fileName}");
        }

        // D($classNode);

        // Find the constant we are looking for
        foreach ($classNode->stmts as $classSubNode) {
            if ($classSubNode instanceof ClassConst) {
                foreach ($classSubNode->consts as $constNode) {
                    if ($constNode->name) {
                        $name = $constNode->name->name;
                        // $this->constants[$name] = $constNode->value;
                        if (is_a($constNode->value, 'PhpParser\Node\Expr\ConstFetch')) {
                            $this->constants[$name] = $constNode->value->name->parts;
                        } elseif (is_a($constNode->value, 'PhpParser\Node\Expr\Array_')) {
                            foreach ($constNode->value->items as $item) {
                                $this->constants[$name][] = $item->value->value;
                            }
                        }

                        // D(get_class($constNode->value));
                    }
                }
            }
        }

        D($this->constants);

        $this->statements = $statements;
    }

    /**
     *
     * @return string
     */
    public function getDeclaringClass()
    {
        return $this->class;
    }

    public function getStatements()
    {
        return $this->statements;
    }

    /**
     *
     * @param  $statements
     * @return Class_
     */
    private function findClassNode($statements)
    {
        foreach ($statements as $node) {
            if ($node instanceof Namespace_) {
                return $this->findClassNode($node->stmts);
            } else {
                if ($node instanceof Class_) {
                    return $node;
                }
            }
        }

        return null;
    }
}
