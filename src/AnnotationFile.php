<?php

namespace Xeriab\Annotations;

/**
 * This class represents a php source-code file inspected for Annotations.
 */
class AnnotationFile
{
    /**
     * Data types considered to be simple.
     *
     * @var array
     */
    private static $simpleTypes = [
        'array',
        'bool',
        'boolean',
        'callback',
        'double',
        'float',
        'int',
        'integer',
        'mixed',
        'number',
        'object',
        'string',
        'void',
    ];

    /**
     *
     * @var array hash where member name => annotation data
     */
    public $data = [];

    /**
     *
     * @var string $path absolute path to php source-file
     */
    private $path = null;

    /**
     *
     * @var string $namespace fully qualified namespace
     */
    private $namespace = null;

    /**
     *
     * @var string[] $uses hash where local class-name => fully qualified class-name
     */
    private $uses = [];

    /**
     *
     * @var string[] $traitMethodOverrides hash mapping FQCN to a hash mapping aliased method names to (trait, original method name)
     */
    private $traitMethodOverrides = [];

    /**
     *
     * @param string $path absolute path to php source-file
     * @param array  $data annotation data (as provided by AnnotationParser)
     */
    public function __construct(string $path = null, array $data = [])
    {
        $this->path = $path;
        $this->data = $data;

        $this->setNamespace($data['#namespace']);
        $this->setUses($data['#uses']);

        if (isset($data['#traitMethodOverrides'])) {
            foreach ($data['#traitMethodOverrides'] as $class => $methods) {
                $this->traitMethodOverrides[$class] = \array_map(
                    [$this, 'resolveMethod'],
                    $methods
                );
            }
        }
    }

    /**
     *
     * @param string $namespace Namespace
     */
    public function setNamespace(string $namespace = null): void
    {
        $this->namespace = $namespace;
    }

    /**
     *
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     *
     * @param mixed $uses Uses
     */
    public function setUses($uses = null): void
    {
        if (! \is_array($uses)) {
            $uses = [$uses];
        }

        foreach ($uses as $key => $value) {
            $this->uses[$key] = $value;
        }
    }

    /**
     *
     * @return string
     */
    public function getUses(): array
    {
        return $this->uses;
    }

    /**
     * Qualify the class name in a method reference like 'Class::method'.
     *
     * @param string $raw_method Raw method string.
     *
     * @return string[] of fully-qualified class name, method name
     */
    public function resolveMethod(string $raw_method = null): array
    {
        list($class, $method) = \explode('::', $raw_method, 2);
        return [\ltrim($this->resolveType($class), '\\'), $method];
    }

    /**
     * Transforms not fully qualified class/interface name into fully qualified one.
     *
     * @param string $raw_type Raw type.
     *
     * @return string
     *
     * @see http://www.phpdoc.org/docs/latest/for-users/phpdoc/types.html#abnf
     */
    public function resolveType(string $raw_type = null): string
    {
        $type_parts = \explode('[]', $raw_type, 2);
        $type = $type_parts[0];

        if (! $this->isSimple($type)) {
            if (isset($this->getUses()[$type])) {
                $type_parts[0] = $this->getUses()[$type];
            } elseif ($this->getNamespace() && \substr($type, 0, 1) != '\\') {
                $type_parts[0] = $this->getNamespace() . '\\' . $type;
            }
        }

        return \implode('[]', $type_parts);
    }

    /**
     * Determines if given data type is scalar.
     *
     * @param string $type Type.
     *
     * @return boolean
     */
    protected function isSimple(string $type = null): bool
    {
        return \in_array(\strtolower($type), self::$simpleTypes);
    }
}
