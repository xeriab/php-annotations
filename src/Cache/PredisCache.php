<?php

namespace Xeriab\Annotations\Cache;

use Xeriab\Annotations\Interfaces\CacheInterface;
use Xeriab\Annotations\Exception\AnnotationException;
use Predis\ClientInterface;
use function array_combine;
use function array_filter;
use function array_map;
use function call_user_func_array;
use function serialize;
use function unserialize;

/**
 * Predis cache provider.
 *
 * @package Xeriab\Annotations
 */
class PredisCache implements CacheInterface
{
    /**
     * {@inheritdoc}
     */
    const IS_FILE = false;

    /** @var ClientInterface */
    private $client = null;

    public function __construct(?ClientInterface $client = null)
    {
        $this->client = $client;
    }

    /**
     * {@inheritdoc}
     */
    public function getKey(string $docBlock = null): string
    {
        return \md5($docBlock);
    }

    /**
     * {@inheritdoc}
     */
    public function set(string $key = null, $data = null, $lifeTime = 0)
    {
        $data = serialize($data);

        if ($lifeTime > 0) {
            $response = $this->client->setex($key, $lifeTime, $data);
        } else {
            $response = $this->client->set($key, $data);
        }

        return $response === true || $response == 'OK';
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key = null)
    {
        $result = $this->client->get($key);

        if ($result === null) {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function exists(string $key = null): bool
    {
        return (bool) $this->client->exists($key);
    }

    /**
     * {@inheritdoc}
     */
    public function fetch(string $key = null)
    {
        return $this->get($key);
    }

    /**
     * {@inheritdoc}
     */
    protected function fetchMultiple(array $keys)
    {
        $fetchedItems = \call_user_func_array([$this->client, 'mget'], $keys);

        return \array_map('unserialize', \array_filter(\array_combine($keys, $fetchedItems)));
    }

    /**
     * {@inheritdoc}
     */
    public function store(string $key = null, $data, $lifeTime = 0)
    {
        $data = serialize($data);

        if ($lifeTime > 0) {
            $response = $this->client->setex($key, $lifeTime, $data);
        } else {
            $response = $this->client->set($key, $data);
        }

        return $response === true || $response == 'OK';
    }

    /**
     * {@inheritdoc}
     */
    public function storeMultiple(array $keysAndValues, $lifetime = 0)
    {
        if ($lifetime) {
            $success = true;

            // Keys have lifetime, use SETEX for each of them
            foreach ($keysAndValues as $key => $value) {
                $response = (string) $this->client->setex($key, $lifetime, \serialize($value));

                if ($response == 'OK') {
                    continue;
                }

                $success = false;
            }

            return $success;
        }

        // No lifetime, use MSET
        $response = $this->client->mset(\array_map(function ($value) {
            return \serialize($value);
        }, $keysAndValues));

        return (string) $response == 'OK';
    }

    /**
     * {@inheritdoc}
     */
    public function delete(string $key = null)
    {
        return $this->client->del($key) >= 0;
    }

    /**
     * {@inheritdoc}
     */
    protected function deleteMultiple(array $keys)
    {
        return $this->client->del($keys) >= 0;
    }

    /**
     * {@inheritdoc}
     */
    public function clear(): void
    {
        $this->client->flushdb();
    }

    /**
     * {@inheritdoc}
     */
    public function flush()
    {
        $response = $this->client->flushdb();

        return $response === true || $response == 'OK';
    }

    /**
     * Retrieves cached information from the data store.
     *
     * The server's statistics array has the following values:
     *
     * - <b>hits</b>
     * Number of keys that have been requested and found present.
     *
     * - <b>misses</b>
     * Number of items that have been requested and not found.
     *
     * - <b>uptime</b>
     * Time that the server is running.
     *
     * - <b>memory_usage</b>
     * Memory used by this server to store items.
     *
     * - <b>memory_available</b>
     * Memory allowed to use for storage.
     *
     * @return array|null An associative array with server's statistics if available, NULL otherwise.
     */
    protected function getStats()
    {
        $info = $this->client->info();

        return [
            CacheInterface::STATS_HITS             => $info['Stats']['keyspace_hits'],
            CacheInterface::STATS_MISSES           => $info['Stats']['keyspace_misses'],
            CacheInterface::STATS_UPTIME           => $info['Server']['uptime_in_seconds'],
            CacheInterface::STATS_MEMORY_USAGE     => $info['Memory']['used_memory'],
            CacheInterface::STATS_MEMORY_AVAILABLE => false,
        ];
    }
}
