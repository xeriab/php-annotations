<?php

namespace Xeriab\Annotations\Cache;

use Xeriab\Annotations\Interfaces\CacheInterface;
use Xeriab\Annotations\Exception\AnnotationException;

/**
 * A file storage cache implementation
 *
 * @package Xeriab\Annotations
 */
class FileCache implements CacheInterface
{
    const IS_FILE = true;

    /**
     *
     * @var string The PHP opening tag (used when writing cache files)
     */
    const PHP_TAG = "<?php\n\n";

    /**
     * Cache storage path
     *
     * @var string
     */
    protected $path = null;

    /**
     *
     * @var int The file mode used when creating new cache files
     */
    private $mode = 0;

    /**
     * Cache entry file extension
     *
     * @var string
     */
    protected $extension = 'annotations';

    /**
     *
     * @param string $path custom sorage path
     */
    public function __construct(string $path = null, int $fileMode = 0777)
    {
        $this->path = $path;
        $this->mode = $fileMode;

        if (! $this->path) {
            $this->path = \sys_get_temp_dir() . '/annotations/';

            if (! \is_dir($this->path)) {
                \mkdir($this->path);
            }
        }

        if (! \is_dir($this->path)
            || ! \is_writable($this->path)
            || ! \is_readable($this->path)
        ) {
            throw new \InvalidArgumentException(
                "Cache path is not a writable/readable " .
                "directory: {$this->path}."
            );
        }
    }

    public function getKey(string $docBlock = null): string
    {
        return \md5($docBlock);
    }

    public function set(string $key = null, array $annotations = []): void
    {
        $file = $this->getFileName($key);

        if (! \file_exists($file)) {
            \file_put_contents($file, \serialize($annotations));
        }
    }

    public function get(string $key = null)
    {
        $file = $this->getFileName($key);

        if (\file_exists($file)) {
            return \unserialize(\file_get_contents($file));
        }

        return [];
    }

    public function clear(): void
    {
        $files = glob(
            $this->path . "*{.{$this->extension}}",
            GLOB_BRACE | GLOB_NOSORT
        );

        foreach ($files as $file) {
            \unlink($file);
        }
    }

    /**
     * Check if annotation-data for the key has been stored.
     *
     * @param string $key cache key
     *
     * @return bool true if data with the given key has been stored; otherwise false
     */
    public function exists(string $key = null): bool
    {
        return \file_exists($this->getFileName($key));
    }

    /**
     * Returns the timestamp of the last cache update for the given key.
     *
     * @param  string $key cache key
     * @return int unix timestamp
     */
    public function getTimestamp(string $key = null): int
    {
        return \filemtime($this->getFileName($key));
    }

    /**
     * Caches the given data with the given key.
     *
     * @param  string $key  cache key
     * @param  array  $code the source-code to be cached
     * @throws AnnotationException if file could not be written
     */
    public function store(string $key = null, $code = null)
    {
        $file = $this->getFileName($key);

        $data = @eval($code);

        // $content = self::PHP_TAG . $code . "\n";
        $content = \serialize($data);

        if (! \file_exists($file)) {
            if (@\file_put_contents($file, $content, \LOCK_EX) === false) {
                throw new AnnotationException(
                    "Unable to write cache file: {$file}"
                );
            }

            if (@\chmod($file, $this->mode) === false) {
                throw new AnnotationException(
                    "Unable to set permissions of cache file: {$file}"
                );
            }
        }
    }

    public function fetch(string $key = null)
    {
        return $this->get($key);
    }

    protected function getFileName(string $key = null): string
    {
        return $this->path . $key . '.' . $this->extension;
    }
}
