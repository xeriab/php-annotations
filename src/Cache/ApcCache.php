<?php

namespace Xeriab\Annotations\Cache;

use Xeriab\Annotations\Interfaces\CacheInterface;

/**
 * Apc cache storage implementation
 *
 * @package Xeriab\Annotations
 * @author  paolo.fagni@gmail.com
 */
class ApcCache implements CacheInterface
{
    const IS_FILE = false;

    /**
     * Cached annotations
     *
     * @var array
     */
    protected $annotations = [];

    public function getKey(string $docBlock = null): string
    {
        return 'annotations:' . \md5($docBlock);
    }

    public function set(string $key = null, array $annotations = []): void
    {
        if (! \apc_exists($key)) {
            \apc_store($key, $annotations);
        }
    }

    public function get(string $key = null)
    {
        if (\apc_exists($key)) {
            return \apc_fetch($key);
        }

        return [];
    }

    public function clear(): void
    {
        $cache = apc_cache_info('user');
        foreach ($cache['cache_list'] as $entry) {
            if (isset($entry['info'])
                && strpos($entry['info'], 'annotations:') === 0
            ) {
                apc_delete($entry['info']);
            }
        }
    }

    public function exists(string $key = null): bool
    {
        return isset($this->annotations[$key]);
    }
}
