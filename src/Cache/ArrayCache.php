<?php

namespace Xeriab\Annotations\Cache;

use Xeriab\Annotations\Interfaces\CacheInterface;

/**
 * A memory storage cache implementation
 *
 * @package Xeriab\Annotations
 */
class ArrayCache implements CacheInterface
{
    /**
     * {@inheritdoc}
     */
    const IS_FILE = false;

    /**
     * Cached annotations
     *
     * @var array
     */
    protected $annotations = [];

    public function getKey(string $docBlock = null): string
    {
        return \md5($docBlock);
    }

    public function set(string $key = null, array $annotations = []): void
    {
        if (! \array_key_exists($key, $this->annotations)) {
            $this->annotations[$key] = $annotations;
        }
    }

    public function get(string $key = null)
    {
        if (\array_key_exists($key, $this->annotations)) {
            return $this->annotations[$key];
        }

        return [];
    }

    public function clear(): void
    {
        $this->annotations = [];
    }

    public function exists(string $key = null): bool
    {
        return isset($this->annotations[$key]);
    }

    /**
     * Caches the given data with the given key.
     *
     * @param  string $key  cache key
     * @param  array  $code the source-code to be cached
     * @throws AnnotationException if file could not be written
     */
    public function store(string $key = null, $code = null)
    {
        $annotations = @eval($code);

        if (! \array_key_exists($key, $this->annotations)) {
            $this->annotations[$key] = $annotations;
        }
    }

    public function fetch(string $key = null)
    {
        return $this->get($key);
    }
}
