<?php

namespace Xeriab\Annotations;

use Xeriab\Annotations\Interfaces\AnnotationInterface;

/**
 * Annotation class
 *
 * @package Annotations
 */
class Annotation implements AnnotationInterface
{
    /**
     * Annotation name.
     *
     * @var string ANNOTATION_NAME.
     */
    const ANNOTATION_NAME = 'Annotation';

    /**
     * Value property. Common among all derived classes.
     *
     * @Ignore
     * @var mixed
     */
    public $value = null;

    /**
     * Constructor.
     *
     * @param array $data Key-value for properties to be defined in this class.
     */
    public function __construct(?array $data = [])
    {
        $isAssoc = \array_keys($data) !== \range(0, \count($data) - 1);

        if ($isAssoc) {
            foreach ($data as $key => $value) {
                $this->{$key} = $value;
            }
        }
    }

    /**
     *
     * @return mixed
     *
     * @since 0.1
     */
    public function get(?string $key = null, $default = null, bool $strict = false)
    {
        if (\property_exists($this, $key)) {
            return $this->$key;
        } elseif (isset($this->$key)) {
            return $this->$key;
        } else {
            if (!\is_null($default)) {
                return $default;
            }

            if ($strict) {
                throw new \RuntimeException(
                    'Unknown property "'.$key.'" of annotation "'.
                    \get_class($this).'".'
                );
            } else {
                return $key;
            }
        }
    }

    /**
     *
     * @since 0.1
     */
    public function set(?string $key = null, $value = null): self
    {
        if (\property_exists($this, $key)) {
            $this->$key = $value;
        } elseif (isset($this->$key)) {
            $this->$key = $value;
        }

        return $this;
    }

    /**
     * Error handler for unknown property accessor in Annotation class.
     *
     * @param string $name Unknown property name.
     *
     * @throws \BadMethodCallException
     */
    public function __get($name)
    {
        throw new \BadMethodCallException(
            \sprintf(
                "Unknown property '%s' on annotation '%s'.",
                $name,
                \get_class($this)
            )
        );
    }

    /**
     * Error handler for unknown property mutator in Annotation class.
     *
     * @param string $name  Unknown property name.
     * @param mixed  $value Property value.
     *
     * @throws \BadMethodCallException
     */
    public function __set($name, $value): void
    {
        throw new \BadMethodCallException(
            \sprintf(
                "Unknown property '%s' on annotation '%s'.",
                $name,
                \get_class($this)
            )
        );
    }

    /**
     * Initializes this annotation instance.
     *
     * @see AnnotationInterface
     */
    public function init(?array $properties = []): void
    {
        foreach ($properties as $name => $value) {
            $this->$name = $value;
        }
    }

    /**
     * Helper method - initializes unnamed properties.
     *
     * @param array &$properties Array of annotation properties, as passed into AnnotationInterface::init()
     * @param array $indexes     Array of unnamed properties
     */
    protected function map(array &$properties = [], array $indexes = []): void
    {
        foreach ($indexes as $index => $name) {
            if (isset($properties[$index])) {
                $this->$name = $properties[$index];
                unset($properties[$index]);
            }
        }
    }

    public static function annotationName(bool $lcFirst = true): string
    {
        $class = \explode('\\', (string) \get_called_class());
        $className = \end($class);
        return ($lcFirst) ? \lcfirst($className): $className;
    }

    public function getIdentifier(): ?Object
    {
        $result = parseClassName(\get_class($this));

        Arr::renameKey($result, 'classname', 'name');

        $result['namespace'] = \implode('\\', $result['namespace']);

        \ksort($result);

        $result = Arr::toObject($result);

        return $result;
    }
}
