<?php

namespace Xeriab\Annotations\Interfaces;

/**
 * This interface is mandatory for all Annotations.
 */
interface AnnotationInterface
{
    public function __get($name);

    public function __set($name, $value): void;

    public function init(?array $properties = []): void;

    public static function annotationName(bool $lcFirst = false): string;

    public function getIdentifier(): ?Object;
}
