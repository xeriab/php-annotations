<?php

namespace Xeriab\Annotations\Interfaces;

/**
 * Interface for annotations Cache
 *
 * @package Annotations
 * @api
 */
interface CacheInterface
{
    public const STATS_HITS             = 'hits';
    public const STATS_MISSES           = 'misses';
    public const STATS_UPTIME           = 'uptime';
    public const STATS_MEMORY_USAGE     = 'memory_usage';
    public const STATS_MEMORY_AVAILABLE = 'memory_available';

    /**
     * Only for backward compatibility (may be removed in next major release)
     *
     * @deprecated
     */
    public const STATS_MEMORY_AVAILIABLE = 'memory_available';

    // const IS_FILE = false;

    /**
     * Generates uuid for a given docblock string
     *
     * @param  string $docBlock docblock string
     * @return string uuid that maps to the given docblock
     */
    public function getKey(string $docBlock = null): string;

    /**
     * Adds an annotation AST to cache
     *
     * @param string $key         cache entry uuid
     * @param array  $annotations annotation AST
     */
    public function set(string $key = null, array $annotations = []);

    /**
     * Retrieves cached annotations from docblock uuid
     *
     * @param  string $key cache entry uuid
     * @return array  cached annotation AST
     */
    public function get(string $key = null);

    public function exists(string $key = null): bool;

    /**
     * Resets cache
     */
    public function clear(): void;
}
