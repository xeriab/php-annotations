<?php

namespace Xeriab\Annotations\Interfaces;

use Xeriab\Annotations\AnnotationFile;

/**
 * This interface mandatory for all Annotations, that require more information about annotation origins.
 */
interface FileAwareInterface
{
    /**
     * Provides information about file, that contains this annotation.
     *
     * @param AnnotationFile $file Annotation file.
     *
     * @return void
     */
    public function setAnnotationFile(AnnotationFile $file = null);
}
