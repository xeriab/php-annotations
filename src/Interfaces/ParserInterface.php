<?php

namespace Xeriab\Annotations\Interfaces;

/**
 * Interface for Parser
 *
 * @package Annotations
 * @api
 */
interface ParserInterface
{
    /**
     * Parses the given docblock string for annotations.
     *
     * @param string $docBlock The docblock string to parse.
     * @param string $context  The parsing context.
     *
     * @return array Array of annotations. If no annotations are found, an empty array is returned.
     */
    public function parse(string $docBlock = null, string $context = '');

    /**
     *
     * @param string $value The raw string value of the Annotation.
     *
     * @return array        An array of Annotation properties.
     */
    public static function parseAnnotation(string $value = null);
}
