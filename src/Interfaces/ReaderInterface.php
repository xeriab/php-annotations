<?php

namespace Xeriab\Annotations\Interfaces;

use Xeriab\Annotations\AnnotationsBag;

/**
 * Interface for annotation reader
 *
 * @package Annotations
 * @api
 */
interface ReaderInterface
{
    /**
     *
     * @param \Xeriab\Annotations\Interfaces\CacheInterface $cache Cache handler
     */
    public function setCache(CacheInterface $cache = null): void;

    /**
     *
     * @return \Xeriab\Annotations\Interfaces\CacheInterface Cache handler
     */
    public function getCache(): CacheInterface;

    /**
     *
     * @param \Xeriab\Annotations\Interfaces\ParserInterface $parser
     */
    public function setParser(ParserInterface $parser = null): void;

    /**
     *
     * @return \Xeriab\Annotations\Interfaces\ParserInterface
     */
    public function getParser(): ParserInterface;

    /**
     * Retrieve all annotations from a given class
     *
     * @param  mixed $class Full qualified class name or object
     * @return \Xeriab\Annotations\AnnotationsBag Annotations collection
     * @throws \ReflectionException                           If class is not found
     */
    public function getClassAnnotations($class = null);

    /**
     * Gets a class annotation.
     *
     * @param mixed  $class          Full qualified class name or object the class annotations should be read.
     * @param string $annotationName The name of the annotation.
     *
     * @return object|null The Annotation or NULL, if the requested annotation does not exist.
     */
    public function getClassAnnotation($class = null, string $annotationName = null);

    /**
     * Retrieve all annotations from a given property of a class
     *
     * @param  mixed  $class    Full qualified class name or object
     * @param  string $property Property name
     * @return \Xeriab\Annotations\AnnotationsBag Annotations collection
     * @throws \ReflectionException                           If property is undefined
     */
    public function getPropertyAnnotations($class = null, string $property = null);

    /**
     * Gets a property annotation.
     *
     * @param \ReflectionProperty $property       The ReflectionProperty to read the annotations from.
     * @param string              $annotationName The name of the annotation.
     *
     * @return object|null The Annotation or NULL, if the requested annotation does not exist.
     */
    public function getPropertyAnnotation(\ReflectionProperty $property, string $annotationName = null);

    /**
     * Retrieve all annotations from a given method of a class
     *
     * @param  mixed  $class  Full qualified class name or object
     * @param  string $method Method name
     * @return \Xeriab\Annotations\AnnotationsBag Annotations collection
     * @throws \ReflectionException                           If method is undefined
     */
    public function getMethodAnnotations($class = null, string $method = null);

    /**
     * Gets a method annotation.
     *
     * @param \ReflectionMethod $method         The ReflectionMethod to read the annotations from.
     * @param string            $annotationName The name of the annotation.
     *
     * @return object|null The Annotation or NULL, if the requested annotation does not exist.
     */
    public function getMethodAnnotation(\ReflectionMethod $method, string $annotationName = null);

    /**
     * Retrieve all annotations from a given function or closure
     *
     * @param  mixed $function Full qualified function name or closure
     * @return \Xeriab\Annotations\AnnotationsBag Annotations collection
     * @throws \ReflectionException                           If function is not found
     */
    public function getFunctionAnnotations($function = null);

    /**
     * Retrieve all annotations from a given constant of a class
     *
     * @param  string|object $class    fully qualified name or instance of the class
     * @param  string        $constant name of the constant
     * @return \Xeriab\Annotations\AnnotationsBag Annotations collection
     */
    public function getConstantAnnotations($class = null, string $constant = null);

    /**
     * Retrieve annotations from docblock of a given reflector
     *
     * @param \Reflector $reflection Reflector object
     * @param bool       $as_bag     Return result as AnnotationsBag if true, array instead.
     *
     * @return \Xeriab\Annotations\AnnotationsBag Annotations collection
     */
    public function getAnnotations(\Reflector $reflection = null, bool $as_bag = true);
}
