<?php

namespace Xeriab\Annotations\Exception;

/**
 * ParserException
 *
 * @package Annotations
 */
class ParserException extends \Exception
{
}
