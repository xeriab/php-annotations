<?php

namespace Xeriab\Annotations\Types;

use Xeriab\Annotations\Interfaces\TypeInterface;

class StringType implements TypeInterface
{
    /**
     * Parse a given value as string
     *
     * @param  string $value
     * @param  null   $annotation Unused
     * @return mixed
     */
    public function parse($value = null, $annotation = null)
    {
        return $value;
    }
}
