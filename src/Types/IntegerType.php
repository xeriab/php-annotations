<?php

namespace Xeriab\Annotations\Types;

use Xeriab\Annotations\Interfaces\TypeInterface;
use Xeriab\Annotations\Exception\ParserException;

class IntegerType implements TypeInterface
{
    /**
     * Filter a value to be an Integer
     *
     * @param  string $value
     * @param  null   $annotation Unused
     * @throws \Xeriab\Annotations\Exception\ParserException
     * @return integer
     */
    public function parse($value = null, $annotation = null)
    {
        if (false === ($value = \filter_var($value, \FILTER_VALIDATE_INT))) {
            throw new ParserException("Raw value must be integer. Invalid value '{$value}' given.");
        }

        return $value;
    }
}
