<?php

namespace Xeriab\Annotations\Types;

use Xeriab\Annotations\Interfaces\TypeInterface;
use Xeriab\Annotations\Exception\ParserException;

class FloatType implements TypeInterface
{
    /**
     * Filter a value to be a Float
     *
     * @param  string $value
     * @param  null   $annotation Unused
     * @throws \Xeriab\Annotations\Exception\ParserException
     * @return float
     */
    public function parse($value = null, $annotation = null)
    {
        if (false === ($value = \filter_var($value, \FILTER_VALIDATE_FLOAT))) {
            throw new ParserException("Raw value must be float. Invalid value '{$value}' given.");
        }

        return $value;
    }
}
