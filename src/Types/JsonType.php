<?php

namespace Xeriab\Annotations\Types;

use Xeriab\Annotations\Interfaces\TypeInterface;

class JsonType implements TypeInterface
{
    /**
     * Filter a value to be a Json
     *
     * @param  string $value
     * @param  null   $annotation Unused
     * @throws \Xeriab\Annotations\Exception\ParserException
     * @return mixed
     */
    public function parse($value = null, $annotation = null)
    {
        $json = \json_decode($value);
        return $json;
    }
}
