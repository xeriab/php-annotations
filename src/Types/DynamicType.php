<?php

namespace Xeriab\Annotations\Types;

use Xeriab\Annotations\Interfaces\TypeInterface;

class DynamicType implements TypeInterface
{
    private function normalizeValue($value)
    {
        $RE = <<<RE
        /
        (?P<start>
            (
                [\\(|\\(\\"]
            )
        )

        (?P<var>
            (
                [a-zA-Z0-9_]
            ).+

            |

            (
                [a-zA-Z0-9_]+[\\=|\\=\\"]+[(.*)+\\"]
            )*+

            |

            (
                [a-zA-Z0-9_]+[\\=|\\=\\"]
            ).+

            |

            (
                [a-zA-Z0-9_][\\=\\{|\\=\\{@]+(.*|[\s\n]+)+[\\}]
            ).+
        )

        (?P<end>
            (
                [\\"\\)|\\)]
            )
        )
        /i
RE;

        $RE = \preg_replace('/\s/', '', $RE);

        return $value;
    }

    /**
     * Parse a given undefined type value
     *
     * @param  string $value
     * @param  null   $annotation Unused
     * @return mixed
     */
    public function parse($value = null, $annotation = null)
    {
        // Implicit boolean
        if ('' === $value) {
            return true;
        }

        if (null !== ($json = \json_decode($value))) {
            return $json;
        } elseif (false !== ($int = \filter_var($value, \FILTER_VALIDATE_INT))) {
            return $int;
        } elseif (false !== ($float = \filter_var($value, \FILTER_VALIDATE_FLOAT))) {
            return $float;
        }

        return $this->normalizeValue($value);
    }
}
