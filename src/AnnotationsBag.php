<?php

namespace Xeriab\Annotations;

use Xeriab\Support\Bag\Bag;

/**
 * An annotation collection class
 *
 * @package Annotations
 */
class AnnotationsBag extends Bag
{
}
