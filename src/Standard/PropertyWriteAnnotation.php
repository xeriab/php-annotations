<?php

namespace Xeriab\Annotations\Standard;

/**
 * Defines a magic/virtual property, it's type and visibility
 */
class PropertyWriteAnnotation extends PropertyAnnotation
{
}
