<?php

namespace Xeriab\Annotations\Standard;

/**
 * Defines a magic/virtual property, it's type and visibility
 */
class PropertyReadAnnotation extends PropertyAnnotation
{
}
