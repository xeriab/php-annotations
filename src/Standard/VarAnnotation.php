<?php

namespace Xeriab\Annotations\Standard;

use Xeriab\Annotations\Interfaces\ParserInterface;
use Xeriab\Annotations\Interfaces\FileAwareInterface;
use Xeriab\Annotations\AnnotationFile;
use Xeriab\Annotations\Exception\AnnotationException;
use Xeriab\Annotations\Annotation;

/**
 * Specifies the required data-type of a property.
 *
 * @usage('property'=>true, 'inherited'=>true)
 */
class VarAnnotation extends Annotation implements
    ParserInterface,
    FileAwareInterface
{
    /**
     *
     * @var string Specifies the type of value (e.g. for validation, for
     * parsing or conversion purposes; case insensitive)
     *
     * The following type-names are recommended:
     *
     *   bool
     *   int
     *   float
     *   string
     *   mixed
     *   object
     *   resource
     *   array
     *   callback (e.g. array($object|$class, $method') or 'function-name')
     *
     * The following aliases are also acceptable:
     *
     *   number (float)
     *   res (resource)
     *   boolean (bool)
     *   integer (int)
     *   double (float)
     */
    public $type = null;

    /**
     * Annotation file.
     *
     * @var AnnotationFile
     */
    protected $file = null;

    /**
     * Parse the standard PHP-DOC annotation
     *
     * @param  string $value
     * @return array
     */
    public static function parseAnnotation(string $value = null)
    {
        $parts = \explode(' ', \trim($value), 2);
        return ['type' => \array_shift($parts)];
    }

    public function parse(?string $docBlock = null, string $content = '')
    {
    }

    /**
     * Initialize the annotation.
     */
    public function init(?array $properties = []): void
    {
        $this->map($properties, ['type']);

        parent::init($properties);

        if (! isset($this->type)) {
            throw new AnnotationException(
                \basename(__CLASS__) . ' requires a type property'
            );
        }

        $this->type = $this->file->resolveType($this->type);
    }

    /**
     * Provides information about file, that contains this annotation.
     *
     * @param AnnotationFile $file Annotation file.
     *
     * @return void
     */
    public function setAnnotationFile(AnnotationFile $file = null)
    {
        $this->file = $file;
    }
}
