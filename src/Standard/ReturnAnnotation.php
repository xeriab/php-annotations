<?php

namespace Xeriab\Annotations\Standard;

use Xeriab\Annotations\Interfaces\ParserInterface;
use Xeriab\Annotations\Interfaces\FileAwareInterface;
use Xeriab\Annotations\AnnotationFile;
use Xeriab\Annotations\Exception\AnnotationException;
use Xeriab\Annotations\Annotation;

/**
 * Defines the return-type of a function or method
 *
 * @usage('method'=>true, 'inherited'=>true)
 */
class ReturnAnnotation extends Annotation implements
    ParserInterface,
    FileAwareInterface
{
    /**
     *
     * @var string
     */
    public $type;

    /**
     * Annotation file.
     *
     * @var AnnotationFile
     */
    protected $file;

    /**
     * Parse the standard PHP-DOC annotation
     *
     * @param  string $value
     * @return array
     */
    public static function parseAnnotation(string $value = null)
    {
        $parts = \explode(' ', \trim($value), 2);

        return ['type' => \array_shift($parts)];
    }

    public function parse(?string $docBlock = null, string $content = '')
    {
    }

    /**
     * Initialize the annotation.
     */
    public function init(?array $properties = []): void
    {
        $this->map($properties, ['type']);

        parent::init($properties);

        if (!isset($this->type)) {
            throw new AnnotationException('ReturnAnnotation requires a type property');
        }

        $this->type = $this->file->resolveType($this->type);
    }

    /**
     * Provides information about file, that contains this annotation.
     *
     * @param AnnotationFile $file Annotation file.
     *
     * @return void
     */
    public function setAnnotationFile(AnnotationFile $file = null)
    {
        $this->file = $file;
    }
}
