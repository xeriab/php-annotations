<?php

namespace Xeriab\Annotations\Standard;

/**
 * Specifies the required data-type of a property.
 */
class TypeAnnotation extends VarAnnotation
{
}
