<?php

namespace Xeriab\Annotations\Standard;

use Xeriab\Annotations\Interfaces\ParserInterface;
use Xeriab\Annotations\Interfaces\FileAwareInterface;
use Xeriab\Annotations\AnnotationFile;
use Xeriab\Annotations\Exception\AnnotationException;
use Xeriab\Annotations\Annotation;

/**
 * Defines a method-parameter's type
 *
 * @usage('method'=>true, 'inherited'=>true, 'multiple'=>true)
 */
class ParamAnnotation extends Annotation implements
    ParserInterface,
    FileAwareInterface
{
    /**
     *
     * @var string
     */
    public $type;

    /**
     *
     * @var string
     */
    public $name;

    /**
     * Annotation file.
     *
     * @var AnnotationFile
     */
    protected $file;

    /**
     * Parse the standard PHP-DOC "param" annotation.
     *
     * @param  string $value
     * @return array ['type', 'name']
     */
    public static function parseAnnotation(string $value = null)
    {
        $parts = \explode(' ', \trim($value), 3);

        if (\count($parts) < 2) {
            // Malformed value, let "init" report about it.
            return [];
        }

        return ['type' => $parts[0], 'name' => \substr($parts[1], 1)];
    }

    public function parse(?string $docBlock = null, string $content = '')
    {
    }

    /**
     * Initialize the annotation.
     */
    public function init(?array $properties = []): void
    {
        $this->map($properties, ['type', 'name']);

        parent::init($properties);

        if (!isset($this->type)) {
            throw new AnnotationException('ParamAnnotation requires a type property');
        }

        if (!isset($this->name)) {
            throw new AnnotationException('ParamAnnotation requires a name property');
        }

        $this->type = $this->file->resolveType($this->type);
    }

    /**
     * Provides information about file, that contains this annotation.
     *
     * @param AnnotationFile $file Annotation file.
     *
     * @return void
     */
    public function setAnnotationFile(AnnotationFile $file = null)
    {
        $this->file = $file;
    }
}
