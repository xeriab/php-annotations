<?php

namespace Xeriab\Annotations;

use SplFileObject;

/**
 * Parses a file for namespaces/use/class declarations.
 *
 * @package Annotations
 */
class PhpParser
{
    /**
     * Parses a class.
     *
     * @param \ReflectionClass $class A `ReflectionClass` object.
     *
     * @return array A list with use statements in the form (Alias => FQN).
     */
    public function parseClass(\ReflectionClass $class = null)
    {
        if (\method_exists($class, 'getUseStatements')) {
            return $class->getUseStatements();
        }

        if (false === $filename = $class->getFileName()) {
            return [];
        }

        $content = $this->getFileContent($filename, $class->getStartLine());

        if (null === $content) {
            return [];
        }

        $namespace = \preg_quote($class->getNamespaceName());

        $content = \preg_replace(
            '/^.*?(\bnamespace\s+' . $namespace . '\s*[;{].*)$/s',
            '\\1',
            $content
        );

        $tokenizer = new TokenParser('<?php ' . $content);

        $statements = $tokenizer->parseUseStatements($class->getNamespaceName());

        return $statements;
    }

    /**
     * Parses a method.
     *
     * @param \ReflectionMethod $method A <code>ReflectionMethod</code> object.
     *
     * @return array A list with use statements in the form (Alias => FQN).
     */
    public function parseMethod(\ReflectionMethod $method = null)
    {
        if (false === $filename = $method->getFileName()) {
            return [];
        }

        $content = $this->getFileContent($filename, $method->getStartLine());

        if (null === $content) {
            return [];
        }
    }

    public function parseFunction(\ReflectionFunction $function = null)
    {
        if (false === $filename = $function->getFileName()) {
            return [];
        }

        $content = $this->getFileContent($filename, $function->getStartLine());

        $tokenizer = new TokenParser('<?php ' . $content);

        $namespace = \preg_quote($function->getNamespaceName());

        $statements = $tokenizer->parseUseStatements($function->getNamespaceName());

        return $statements;
    }

    /**
     * Gets the content of the file right up to the given line number.
     *
     * @param string  $filename   The name of the file to load.
     * @param integer $lineNumber The number of lines to read from file.
     *
     * @return string|null The content of the file or null if the file does not exist.
     */
    private function getFileContent($filename, $lineNumber)
    {
        if (!\is_file($filename)) {
            return null;
        }

        $content = '';
        $count   = 0;
        $file    = new SplFileObject($filename);

        while (!$file->eof()) {
            if (++$count == $lineNumber) {
                break;
            }

            $content .= $file->fgets();
        }

        return $content;
    }
}
