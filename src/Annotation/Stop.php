<?php

namespace Xeriab\Annotations\Annotation;

use Xeriab\Annotations\Annotation;

/**
 * This Annotation indicates that parent class should not be parsed for annotations.
 *
 * @usage('class'=>true)
 */
class Stop extends Annotation
{
}
