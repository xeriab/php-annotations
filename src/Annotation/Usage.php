<?php

namespace Xeriab\Annotations\Annotation;

use Xeriab\Annotations\Annotation;

/**
 * This Annotation is mandatory, and must be applied to all Annotations.
 */
class Usage extends Annotation
{
    /**
     *
     * @var boolean Set this to TRUE for Annotations that may be applied to classes.
     */
    public $class = false;

    /**
     *
     * @var boolean Set this to TRUE for Annotations that may be applied to properties.
     */
    public $property = false;

    /**
     *
     * @var boolean Set this to TRUE for Annotations that may be applied to methods.
     */
    public $method = false;

    /**
     *
     * @var boolean $multiple Set this to TRUE for Annotations that allow multiple instances on the same member.
     */
    public $multiple = false;

    /**
     *
     * @var boolean $inherited Set this to TRUE for Annotations that apply to members of child classes.
     */
    public $inherited = false;
}
