<?php

namespace Xeriab\Annotations\Annotation;

/**
 * Annotation that can be used to signal to the parser
 * to check the annotation target during the parsing process.
 *
 * @Annotation
 */
final class Target
{
    const TARGET_CLASS      = 1;
    const TARGET_METHOD     = 2;
    const TARGET_PROPERTY   = 4;
    const TARGET_ANNOTATION = 8;
    const TARGET_FUNCTION   = 16;
    const TARGET_CONSTANT   = 32;
    const TARGET_OBJECT     = 64;
    const TARGET_ANY        = 128;
    const TARGET_ENUM       = 256;
    const TARGET_ALL        = 511;

    /**
     *
     * @var array
     */
    private static $map = [
        'ANY'        => self::TARGET_ANY,
        'ALL'        => self::TARGET_ALL,
        'CLASS'      => self::TARGET_CLASS,
        'METHOD'     => self::TARGET_METHOD,
        'PROPERTY'   => self::TARGET_PROPERTY,
        'FUNCTION'   => self::TARGET_FUNCTION,
        'CONSTANT'   => self::TARGET_CONSTANT,
        'OBJECT'     => self::TARGET_OBJECT,
        'ENUM'       => self::TARGET_ENUM,
        'ANNOTATION' => self::TARGET_ANNOTATION,

        'Any'        => self::TARGET_ANY,
        'All'        => self::TARGET_ALL,
        'Class'      => self::TARGET_CLASS,
        'Method'     => self::TARGET_METHOD,
        'Property'   => self::TARGET_PROPERTY,
        'Function'   => self::TARGET_FUNCTION,
        'Constant'   => self::TARGET_CONSTANT,
        'Object'     => self::TARGET_OBJECT,
        'Enum'       => self::TARGET_ENUM,
        'Annotation' => self::TARGET_ANNOTATION,

        '*'          => self::TARGET_ALL,
    ];

    /**
     *
     * @var array
     */
    public $value = [];

    /**
     * Targets as bitmask.
     *
     * @var integer
     */
    public $targets = 0;

    /**
     * Literal target declaration.
     *
     * @var integer
     */
    public $literal = 0;

    /**
     * Annotation constructor.
     *
     * @param array $values
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(array $values = [])
    {
        if (! isset($values['value'])) {
            $values['value'] = null;
        }

        if (\is_string($values['value'])) {
            $values['value'] = [$values['value']];
        }

        if (! \is_array($values['value'])) {
            throw new \InvalidArgumentException(
                \sprintf(
                    '@Target expects either a string value, or an array of ' .
                    'strings, "%s" given.',
                    \is_object($values['value']) ?
                        \get_class($values['value']) :
                        \gettype($values['value'])
                )
            );
        }

        $bitmask = 0;

        foreach ($values['value'] as $literal) {
            if (! isset(self::$map[$literal])) {
                throw new \InvalidArgumentException(
                    \sprintf(
                        'Invalid Target "%s". Available targets: [%s]',
                        $literal,
                        \implode(', ', \array_keys(self::$map))
                    )
                );
            }

            $bitmask |= self::$map[$literal];
        }

        $this->targets  = $bitmask;
        $this->value    = $values['value'];
        $this->literal  = \implode(', ', $this->value);
    }
}
