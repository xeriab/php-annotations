<?php

declare(strict_types=1);

namespace Xeriab\Annotations;

use Xeriab\Annotations\Cache\ArrayCache;
use Xeriab\Annotations\Interfaces\CacheInterface;
use Xeriab\Annotations\Interfaces\ParserInterface;
use Xeriab\Annotations\Interfaces\ReaderInterface;
use Xeriab\Annotations\Annotation\Usage;
use Xeriab\Annotations\Exception\AnnotationException;
use Xeriab\Annotations\Annotation\IgnoreAnnotation;
use Xeriab\Annotations\Annotation\Target;
use Xeriab\Annotations\AnnotationsBag as Bag;
use Xeriab\Support\Interfaces\Singletonable;
use Xeriab\Support\Str;
use Xeriab\Support\Arr;

/**
 * This class is the primary entry point to read annotations
 *
 * @package Xeriab\Annotations
 */
class Reader implements ReaderInterface, Singletonable
{
    const CACHE_FORMAT_VERSION = 3;
    const MEMBER_CLASS         = 'class';
    const MEMBER_METHOD        = 'method';
    const MEMBER_PROPERTY      = 'property';

    /**
     *
     * @var \Xeriab\Annotations\Interfaces\ParserInterface
     */
    protected $parser = null;

    /**
     * Annotations parser used to collect parsing metadata.
     *
     * @var \Xeriab\Annotations\DocParser
     */
    private $preParser = null;

    /**
     * PHP parser used to collect imports.
     *
     * @var \Xeriab\Annotations\PhpParser
     */
    private $phpParser = null;

    /**
     *
     * @var \Xeriab\Annotations\Interfaces\CacheInterface
     */
    protected $cache = null;

    /**
     *
     * @var boolean Enable PHP autoloader when searching for annotation classes (defaults to true)
     */
    private $autoload = true;

    /**
     *
     * @var string The class-name suffix for Annotation classes.
     */
    private $suffix = 'Annotation';

    /**
     *
     * @var string The default namespace for annotations with no namespace qualifier.
     */
    private $namespace = '';

    /**
     * In-memory cache mechanism to store imported annotations per class.
     *
     * @var array
     */
    private $imports = [];

    private $namedResult = false;

    /**
     * In-memory cache mechanism to store ignored annotations per class.
     *
     * @var array
     */
    private $ignoredAnnotationNames = [];

    /**
     *
     * @var array $annotationRegistry List of registered annotation aliases.
     */
    private static $annotationRegistry = [
        // Annotation tags
        'Annotation'              => false,
        'Attribute'               => false,
        'Attributes'              => false,
        /* Can we enable this? 'Enum'  => false, */
        'Required'                => false,
        'Target'                  => false,
        // Widely used tags (but not existent in cd)
        'fix'                     => false,
        'fixme'                   => false,
        'override'                => false,
        // PHPDocumentor 1 tags
        'abstract'                => false,
        'access'                  => false,
        'code'                    => false,
        'deprec'                  => false,
        'endcode'                 => false,
        'exception'               => false,
        'final'                   => false,
        'ingroup'                 => false,
        'inheritdoc'              => false,
        'inheritDoc'              => false,
        'magic'                   => false,
        'name'                    => false,
        'toc'                     => false,
        'tutorial'                => false,
        'private'                 => false,
        'static'                  => false,
        'staticvar'               => false,
        'staticVar'               => false,
        'throw'                   => false,
        // PHPDocumentor 2 tags.
        'api'                     => false,
        'author'                  => false,
        'category'                => false,
        'copyright'               => false,
        'deprecated'              => false,
        'example'                 => false,
        'filesource'              => false,
        'global'                  => false,
        /* Can we enable this? 'index' => false, */
        'ignore'                  => false,
        'internal'                => false,
        'license'                 => false,
        'link'                    => false,
        'method'                  => 'Xeriab\Annotations\Standard\MethodAnnotation',
        'package'                 => false,
        'param'                   => 'Xeriab\Annotations\Standard\ParamAnnotation',
        'property'                => 'Xeriab\Annotations\Standard\PropertyAnnotation',
        'property-read'           => 'Xeriab\Annotations\Standard\PropertyReadAnnotation',
        'property-write'          => 'Xeriab\Annotations\Standard\PropertyWriteAnnotation',
        'return'                  => 'Xeriab\Annotations\Standard\ReturnAnnotation',
        'see'                     => false,
        'since'                   => false,
        'source'                  => false,
        'subpackage'              => false,
        'throws'                  => false,
        'todo'                    => false,
        'TODO'                    => false,
        'usedby'                  => false,
        'uses'                    => false,
        'var'                     => 'Xeriab\Annotations\Standard\VarAnnotation',
        'type'                    => 'Xeriab\Annotations\Standard\TypeAnnotation',
        'stop'                    => 'Xeriab\Annotations\Annotation\Stop',
        'usage'                   => 'Xeriab\Annotations\Annotation\Usage',
        'version'                 => false,
        // PHPUnit tags
        'codeCoverageIgnore'      => false,
        'codeCoverageIgnoreStart' => false,
        'codeCoverageIgnoreEnd'   => false,
        // PHPCheckStyle
        'SuppressWarnings'        => false,
        // PHPStorm
        'noinspection'            => false,
        // PEAR
        'package_version'         => false,
        // PlantUML
        'startuml'                => false,
        'enduml'                  => false,
        // Symfony 3.3 Cache Adapter
        'experimental'            => false,

        // 'Test'                    => false,
    ];

    /**
     * Global map for imports.
     *
     * @var array
     */
    private static $globalImports = [
        'ignoreannotation' => 'Xeriab\Annotations\Annotation\IgnoreAnnotation',
    ];

    /**
     * A list with annotations that are not causing exceptions when not resolved to an annotation class.
     *
     * The names are case sensitive.
     *
     * @var array
     */
    private static $globalIgnoredNames = [
        // Annotation tags
        'Annotation'              => true,
        'Attribute'               => true,
        'Attributes'              => true,
        /* Can we enable this? 'Enum'  => true, */
        'Required'                => true,
        'Target'                  => true,
        // Widely used tags (but not existent in cd)
        'fix'                     => true,
        'fixme'                   => true,
        'override'                => true,
        // PHPDocumentor 1 tags
        'abstract'                => true,
        'access'                  => true,
        'code'                    => true,
        'deprec'                  => true,
        'endcode'                 => true,
        'exception'               => true,
        'final'                   => true,
        'ingroup'                 => true,
        'inheritdoc'              => true,
        'inheritDoc'              => true,
        'magic'                   => true,
        'name'                    => true,
        'toc'                     => true,
        'tutorial'                => true,
        'private'                 => true,
        'static'                  => true,
        'staticvar'               => true,
        'staticVar'               => true,
        'throw'                   => true,
        // PHPDocumentor 2 tags.
        'api'                     => true,
        'author'                  => true,
        'category'                => true,
        'copyright'               => true,
        'deprecated'              => true,
        'example'                 => true,
        'filesource'              => true,
        'global'                  => true,
        /* Can we enable this? 'index' => true, */
        'ignore'                  => true,
        'internal'                => true,
        'license'                 => true,
        'link'                    => true,
        'method'                  => true,
        'package'                 => true,
        'param'                   => true,
        'property'                => true,
        'property-read'           => true,
        'property-write'          => true,
        'return'                  => true,
        'see'                     => true,
        'since'                   => true,
        'source'                  => true,
        'subpackage'              => true,
        'throws'                  => true,
        'todo'                    => true,
        'TODO'                    => true,
        'usedby'                  => true,
        'uses'                    => true,
        'var'                     => true,
        'type'                    => true,
        'stop'                    => true,
        'usage'                   => true,
        'version'                 => true,
        // PHPUnit tags
        'codeCoverageIgnore'      => true,
        'codeCoverageIgnoreStart' => true,
        'codeCoverageIgnoreEnd'   => true,
        // PHPCheckStyle
        'SuppressWarnings'        => true,
        // PHPStorm
        'noinspection'            => true,
        // PEAR
        'package_version'         => true,
        // PlantUML
        'startuml'                => true,
        'enduml'                  => true,
        // Symfony 3.3 Cache Adapter
        'experimental'            => true,

        // 'Test'                    => true,
    ];

    /**
     * A list with annotations that are not causing exceptions when not resolved to an annotation class.
     *
     * The names are case sensitive.
     *
     * @var array
     */
    private static $globalIgnoredNamespaces = [];

    /**
     * An internal cache for annotation-data loaded from source-code files
     *
     * @var AnnotationFile[] hash where absolute path to php source-file => AnnotationFile instance
     */
    protected $files = [];

    /**
     *
     * @var array[] An internal cache for Annotation instances
     * @see getAnnotations()
     */
    protected $annotations = [];

    /**
     *
     * @var bool[] An array of flags indicating which annotation sets have been initialized
     * @see getAnnotations()
     */
    protected $initialized = [];

    /**
     *
     * @var Usage[] An internal cache for Usage instances
     */
    protected $usage = [];

    /**
     *
     * @var Usage The standard Usage
     */
    private $usageAnnotation;

    /**
     *
     * @var string $cacheSeed A seed for caching - used when generating cache keys, to prevent collisions
     *                        when using more than one Reader in the same application.
     */
    private $cacheSeed = '';

    /**
     * Whether this version of PHP has support for traits.
     */
    private $isTraitsSupported = false;

    /**
     *
     * @var boolean $debug Set to TRUE to enable HTML output for debugging
     */
    private $debug = false;

    /**
     * Reader instance.
     *
     * @var \Xeriab\Annotations\Reader
     */
    private static $instance = null;

    /**
     * Constructor.
     *
     * @param ParserInterface|null $parser    Annotation parser
     * @param CacheInterface|null  $cache     Cache handler
     * @param bool|boolean         $debug
     * @param bool|boolean         $autoload
     * @param string               $suffix
     * @param string               $namespace
     *
     * @return void
     */
    public function __construct(
        ParserInterface $parser = null,
        CacheInterface $cache = null,
        bool $debug = false,
        bool $autoload = false,
        string $suffix = 'Annotation',
        string $namespace = ''
    ) {
        if (
            \extension_loaded('Zend Optimizer+')
            && (\ini_get('zend_optimizerplus.save_comments') === "0"
                || \ini_get('opcache.save_comments') === "0")
        ) {
            throw AnnotationException::optimizerPlusSaveComments();
        }

        if (
            \extension_loaded('Zend OPcache')
            && \ini_get('opcache.save_comments') == 0
        ) {
            throw AnnotationException::optimizerPlusSaveComments();
        }

        if (\PHP_VERSION_ID < 70000) {
            if (
                \extension_loaded('Zend Optimizer+')
                && (\ini_get('zend_optimizerplus.load_comments') === "0"
                    || \ini_get('opcache.load_comments') === "0")
            ) {
                throw AnnotationException::optimizerPlusLoadComments();
            }

            if (
                \extension_loaded('Zend OPcache')
                && \ini_get('opcache.load_comments') == 0
            ) {
                throw AnnotationException::optimizerPlusLoadComments();
            }
        }

        AnnotationRegistry::registerFile(
            __DIR__ . '/Annotation/IgnoreAnnotation.php'
        );

        // AnnotationRegistry::registerLoader('class_exists');

        $this->setDebug($debug);
        $this->setAutoload($autoload);
        $this->setSuffix($suffix);
        $this->setNamespace($namespace);

        $this->setParser($parser);
        $this->setCache($cache);

        $this->usageAnnotation = new Usage();
        $this->usageAnnotation->class = true;
        $this->usageAnnotation->inherited = true;

        $this->isTraitsSupported = \version_compare(\PHP_VERSION, '5.4.0', '>=');

        static::$instance = &$this;
    }

    /**
     * Returns the ignored annotations for the given class.
     *
     * @param \ReflectionClass $class
     *
     * @return array
     */
    private function getIgnoredAnnotationNames(\Reflector $reflection)
    {
        // $ref = \method_exists($reflection, 'getDeclaringClass') ?
        //     $reflection->getDeclaringClass() :
        //     $reflection;

        $target = \get_class($reflection);

        if (Str::contains($target, '\\')) {
            $target = \explode('\\', $target);
            $target = \end($target);
        }

        $targetType = \strtolower(\substr($target, 10));

        $name = $reflection->getName();

        if (isset($this->ignoredAnnotationNames[$name])) {
            return $this->ignoredAnnotationNames[$name];
        }

        // $this->collectParsingMetadata($class);

        switch ($targetType) {
            case 'property':
            case 'class':
            case 'method':
            case 'classconstant':
            case 'constant':
                $this->collectParsingMetadata($reflection);
                break;
            case 'function':
                $this->collectFunctionParsingMetadata($reflection);
                break;
        }

        return $this->ignoredAnnotationNames[$name];
    }

    /**
     * Retrieves imports.
     *
     * @param \ReflectionClass $class
     *
     * @return array
     */
    private function getClassImports(\ReflectionClass $class)
    {
        $name = $class->getName();

        if (isset($this->imports[$name])) {
            return $this->imports[$name];
        }

        $this->collectParsingMetadata($class);

        return $this->imports[$name];
    }

    /**
     * Retrieves imports for methods.
     *
     * @param \ReflectionMethod $method
     *
     * @return array
     */
    private function getMethodImports(\ReflectionMethod $method)
    {
        $class = $method->getDeclaringClass();

        $classImports = $this->getClassImports($class);

        if (!\method_exists($class, 'getTraits')) {
            return $classImports;
        }

        $traitImports = [];

        foreach ($class->getTraits() as $trait) {
            if (
                $trait->hasMethod($method->getName())
                && $trait->getFileName() === $method->getFileName()
            ) {
                $traitImports = \array_merge(
                    $traitImports,
                    $this->phpParser->parseClass($trait)
                );
            }
        }

        return \array_merge($classImports, $traitImports);
    }

    /**
     * Retrieves imports for constants.
     *
     * @param \ReflectionClassConstant $constant
     *
     * @return array
     */
    private function getConstantImports(\ReflectionClassConstant $constant)
    {
        $class = $constant->getDeclaringClass();

        $classImports = $this->getClassImports($class);

        if (!\method_exists($class, 'getTraits')) {
            return $classImports;
        }

        $traitImports = [];

        foreach ($class->getTraits() as $trait) {
            if (
                $trait->hasConstant($constant->getName())
                && $trait->getFileName() === $constant->getFileName()
            ) {
                $traitImports = \array_merge(
                    $traitImports,
                    $this->phpParser->parseClass($trait)
                );
            }
        }

        return \array_merge($classImports, $traitImports);
    }

    /**
     * Retrieves imports for functions.
     *
     * @param \ReflectionFunction $function
     *
     * @return array
     */
    private function getFunctionImports(\ReflectionFunction $function)
    {
        return $this->phpParser->parseFunction($function);
    }

    /**
     * Retrieves imports for properties.
     *
     * @param \ReflectionProperty $property
     *
     * @return array
     */
    private function getPropertyImports(\ReflectionProperty $property)
    {
        $class = $property->getDeclaringClass();

        $classImports = $this->getClassImports($class);

        if (!\method_exists($class, 'getTraits')) {
            return $classImports;
        }

        $traitImports = [];

        foreach ($class->getTraits() as $trait) {
            if ($trait->hasProperty($property->getName())) {
                $traitImports = \array_merge(
                    $traitImports,
                    $this->phpParser->parseClass($trait)
                );
            }
        }

        return \array_merge($classImports, $traitImports);
    }

    /**
     * Collects parsing metadata for a given class.
     *
     * @param mixed $class Full qualified class name or object
     *
     * @return void
     */
    private function collectParsingMetadata($class = null)
    {
        if (!\is_null($class) && \is_string($class)) {
            $class = new \ReflectionClass($class);
        }

        $ignoredAnnotationNames = static::$globalIgnoredNames;
        $annotations            = $this->preParser->parse(
            $class->getDocComment(),
            'class ' . $class->name
        );

        foreach ($annotations as $annotation) {
            if ($annotation instanceof IgnoreAnnotation) {
                foreach ($annotation->names as $annot) {
                    $ignoredAnnotationNames[$annot] = true;
                }
            }
        }

        $name = $class->getName();

        $this->imports[$name] = \array_merge(
            static::$globalImports,
            $this->phpParser->parseClass($class),
            ['__NAMESPACE__' => $class->getNamespaceName()]
        );

        $this->ignoredAnnotationNames[$name] = $ignoredAnnotationNames;
    }

    /**
     * Collects parsing metadata for a given function.
     *
     * @param mixed $function Full qualified function name.
     *
     * @return void
     */
    private function collectFunctionParsingMetadata($function = null)
    {
        if (!\is_null($function) && \is_string($function)) {
            $function = new \ReflectionFunction($function);
        }

        $ignoredAnnotationNames = static::$globalIgnoredNames;
        $annotations            = $this->preParser->parse(
            $function->getDocComment(),
            'function ' . $function->name
        );

        foreach ($annotations as $annotation) {
            if ($annotation instanceof IgnoreAnnotation) {
                foreach ($annotation->names as $annot) {
                    $ignoredAnnotationNames[$annot] = true;
                }
            }
        }

        $name = $function->getName();

        $this->imports[$name] = \array_merge(
            static::$globalImports,
            $this->phpParser->parseFunction($function),
            ['__NAMESPACE__' => $function->getNamespaceName()]
        );

        $this->ignoredAnnotationNames[$name] = $ignoredAnnotationNames;
    }

    /**
     * Returns the Reader instance.
     *
     * @return \Xeriab\Annotations\Reader
     */
    public static function getInstance(): ?self
    {
        return static::$instance;
    }

    /**
     * Reader Singleton Factory.
     *
     * @return \Xeriab\Annotations\Reader
     */
    public static function instance(): ?self
    {
        if (null === static::$instance) {
            static::$instance = static::createFromDefaults();
        }

        return static::$instance;
    }

    /**
     * Register/Add annotation.
     *
     * @param string $name
     * @param mixed  $handler
     */
    public static function registerAnnotaion(
        string $name = null,
        $handler = null
    ) {
        static::$annotationRegistry[$name] = $handler;
    }

    /**
     * Gets Registered/Added annotations.
     */
    public static function getRegisteredAnnotaions(): array
    {
        return static::$annotationRegistry;
    }

    /**
     * Add a new annotation to the globally ignored annotation names with regard to exception handling.
     *
     * @param string $name
     */
    public static function addGlobalIgnoredName(string $name = null)
    {
        static::$globalIgnoredNames[$name] = true;
    }

    /**
     * Add a new annotation to the globally ignored annotation namespaces with regard to exception handling.
     *
     * @param string $namespace
     */
    public static function addGlobalIgnoredNamespace(string $namespace = null)
    {
        static::$globalIgnoredNamespaces[$namespace] = true;
    }

    /**
     * Retrieves annotation-data from a given source-code file.
     *
     * Member-names in the returned array have the following format: Class, Class::method or Class::$member
     *
     * @param string $path the path of the source-code file from which to obtain annotation-data.
     *
     * @return AnnotationFile
     *
     * @throws AnnotationException if cache is not configured
     *
     * @see $files
     * @see $cache
     */
    protected function getAnnotationFile(string $path = null)
    {
        if (!isset($this->files[$path])) {
            if ($this->getCache() === null) {
                throw new AnnotationException(
                    "Reader::\$cache is not configured"
                );
            }

            if ($this->getCache() === false) {
                // Caching is disabled
                $code = $this->getParser()->parseFile($path);
                $data = eval($code);
            } else {
                $checksum = \crc32(
                    $path . ':' . $this->cacheSeed .
                        ':' . static::CACHE_FORMAT_VERSION
                );

                $key = \basename($path) . '-' . \sprintf('%x', $checksum);

                if (($this->getCache()->exists($key) === false)
                    || (\filemtime($path) > $this->getCache()->getTimestamp($key))
                ) {
                    $code = $this->getParser()->parseFile($path);
                    $this->getCache()->store($key, $code);
                }

                $data = $this->getCache()->fetch($key);
            }

            $this->files[$path] = new AnnotationFile($path, $data);
        }

        return $this->files[$path];
    }

    /**
     * Resolves a name, using built-in annotation name resolution rules, and the registry.
     *
     * @param string $name The annotation name.
     *
     * @return string      The fully qualified annotation class-name, or false if the
     *                     requested annotation has been disabled (set to false) in
     *                     the registry.
     *
     * @see $annotationRegistry
     */
    public function resolveName(string $name = null)
    {
        if (\strpos($name, '\\') !== false) {
            // Annotation class-name is fully qualified
            return $name . $this->getSuffix();
        }

        $type = \lcfirst($name);

        if (isset(static::$annotationRegistry[$type])) {
            // type-name is registered
            return static::$annotationRegistry[$type];
        }

        $type = \ucfirst(\strtr($name, '-', '_')) . $this->getSuffix();

        return \strlen($this->getNamespace())
            ? $this->getNamespace() . '\\' . $type
            : $type;
    }

    /**
     * Filters annotations by class name.
     *
     * @param AnnotationInterface[] $annotations An array of annotation objects
     * @param string                $type        The class-name by which to filter annotation objects;
     *                                           or annotation type-name with a leading "@", e.g.
     *                                           "@var", which will be resolved through the registry.
     *
     * @return array                             The filtered array of annotation objects - may return an empty array.
     */
    protected function filterAnnotations(
        array $annotations = [],
        string $type = null
    ): array {
        if (\substr($type, 0, 1) === '@') {
            $type = $this->resolveName(\substr($type, 1));
        }

        if ($type === false) {
            return [];
        }

        $result = [];

        foreach ($annotations as $annotation) {
            if ($annotation instanceof $type) {
                $result[] = $annotation;
            }
        }

        return $result;
    }

    /**
     * Obtain the Usage for a given Annotation class.
     *
     * @param string $class The Annotation type class-name
     *
     * @return Usage
     *
     * @throws AnnotationException if the given class-name is invalid; if the annotation-type has no defined usage
     */
    public function getUsage(string $class = null)
    {
        if ($class === static::$annotationRegistry['usage']) {
            return $this->usageAnnotation;
        }

        if (!isset($this->usage[$class])) {
            if (!\class_exists($class, $this->getAutoload())) {
                throw new AnnotationException(
                    "Annotation type '{$class}' does not exist"
                );
            }

            $usage = $this->getAnnotations($class);

            if (\count($usage) === 0) {
                throw new AnnotationException(
                    "The class '{$class}' must have exactly one Usage"
                );
            } else {
                if (
                    \count($usage) !== 1
                    || !($usage[0] instanceof Usage)
                ) {
                    throw new AnnotationException(
                        "The class '{$class}' must have exactly one " .
                            "Usage (no other Annotations are allowed)"
                    );
                } else {
                    $usage = $usage[0];
                }
            }

            $this->usage[$class] = $usage;
        }

        return $this->usage[$class];
    }

    /**
     *
     * @param \Xeriab\Annotations\Interfaces\CacheInterface $cache Cache handler
     */
    public function setCache(CacheInterface $cache = null): void
    {
        $this->cache = $cache ?: new ArrayCache();
    }

    /**
     *
     * @return \Xeriab\Annotations\Interfaces\CacheInterface Cache handler
     */
    public function getCache(): CacheInterface
    {
        return $this->cache;
    }

    /**
     *
     * @param bool|boolean $debug Debug
     */
    public function setDebug(bool $debug = false): void
    {
        $this->debug = $debug ?: false;
    }

    /**
     *
     * @param bool|boolean $namedResult Allow named result
     */
    public function setNamedResult(bool $namedResult = false): void
    {
        $this->namedResult = $namedResult ?: false;
    }

    /**
     *
     * @return bool|boolean
     */
    public function getDebug(): bool
    {
        return $this->debug;
    }

    /**
     *
     * @param bool|boolean $autoload Autoload
     */
    public function setAutoload(bool $autoload = false): void
    {
        $this->autoload = $autoload ?: false;
    }

    /**
     *
     * @return bool|boolean
     */
    public function getAutoload(): bool
    {
        return $this->autoload;
    }

    /**
     *
     * @param string $namespace Namespace
     */
    public function setNamespace(string $namespace = null): void
    {
        $this->namespace = $namespace ?: '';
    }

    /**
     *
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     *
     * @param string $suffix Suffix
     */
    public function setSuffix(string $suffix = null): void
    {
        $this->suffix = $suffix ?: 'Annotation';
    }

    /**
     *
     * @return string
     */
    public function getSuffix(): string
    {
        return $this->suffix;
    }

    /**
     *
     * @param \Xeriab\Annotations\Interfaces\ParserInterface $parser
     */
    public function setParser(ParserInterface $parser = null): void
    {
        $this->parser = $parser ?: new DocParser();
        $this->preParser = new DocParser;
        $this->preParser->setImports(static::$globalImports);
        $this->preParser->setIgnoreNotImportedAnnotations(true);
        $this->phpParser = new PhpParser;
    }

    /**
     *
     * @return \Xeriab\Annotations\Interfaces\ParserInterface
     */
    public function getParser(): ParserInterface
    {
        return $this->parser;
    }

    /**
     * Retrieve all properties annotations from a given class.
     *
     * @param mixed $class          Full qualified class name or object.
     * @param bool  $include_static Include static properties.
     *
     * @return \Xeriab\Annotations\AnnotationsBag|array|mixed Annotations collection.
     */
    public function getClassPropertiesAnnotations(
        $class = null,
        bool $include_static = false
    ) {
        if (\is_string($class)) {
            $class = new \ReflectionClass($class);
        }

        $result = [];

        $flags = \ReflectionProperty::IS_PROTECTED | \ReflectionProperty::IS_PRIVATE;

        if ($include_static) {
            $flags |= \ReflectionProperty::IS_STATIC;
        }

        // Grap the class properites
        $properties = $class->getProperties($flags);

        Arr::map(
            $properties,
            function ($key, $value) use (&$result, &$class, &$include_static) {
                if ($include_static) {
                    $result[$value->getName()] = $this->getPropertyAnnotations(
                        $class->getName(),
                        $value->getName()
                    );
                } elseif (!$value->isStatic()) {
                    $result[$value->getName()] = $this->getPropertyAnnotations(
                        $class->getName(),
                        $value->getName()
                    );
                }
            }
        );

        if (\defined($class->getName() . '::FIELDS')) {
            $result = Arr::sortByArray(
                $result,
                \constant($class->getName() . '::FIELDS')
            );
        }

        // D($result);

        return ($result) ? new AnnotationsBag($result) : null;
    }

    /**
     * Retrieve all annotations from a given class
     *
     * @param  mixed $class Full qualified class name or object
     * @return \Xeriab\Annotations\AnnotationsBag|array|mixed|array|mixed Annotations collection
     * @throws \ReflectionException                           If class is not found
     */
    public function getClassAnnotations($class = null)
    {
        if (\is_string($class)) {
            $class = new \ReflectionClass($class);
        }

        return $this->getAnnotations($class);
    }

    /**
     * {@inheritDoc}
     */
    public function getClassAnnotation(
        $class = null,
        string $annotationName = null
    ) {
        $annotations = $this->getClassAnnotations($class);

        if (!empty($annotations)) {
            foreach ($annotations as $annotation) {
                if (\class_exists($annotationName)) {
                    if ($annotation instanceof $annotationName) {
                        return $annotation;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Retrieve all annotations from a given property of a class
     *
     * @param  mixed  $class    Full qualified class name or object
     * @param  string $property Property name
     * @return \Xeriab\Annotations\AnnotationsBag|array|mixed Annotations collection
     * @throws \ReflectionException                           If property is undefined
     */
    public function getPropertyAnnotations(
        $class = null,
        string $property = null
    ) {
        return $this->getAnnotations(
            new \ReflectionProperty($class, $property)
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getPropertyAnnotation(
        \ReflectionProperty $property,
        string $annotationName = null
    ) {
        $annotations = $this->getPropertyAnnotations(
            $property->getDeclaringClass()->getName(),
            $property->name
        );

        if (!empty($annotations)) {
            foreach ($annotations as $annotation) {
                if (\class_exists($annotationName)) {
                    if ($annotation instanceof $annotationName) {
                        return $annotation;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Retrieve all annotations from a given method of a class
     *
     * @param  mixed  $class  Full qualified class name or object
     * @param  string $method Method name
     * @return \Xeriab\Annotations\AnnotationsBag|array|mixed Annotations collection
     * @throws \ReflectionException                           If method is undefined
     */
    public function getMethodAnnotations(
        $class = null,
        string $method = null
    ) {
        return $this->getAnnotations(new \ReflectionMethod($class, $method));
    }

    /**
     * {@inheritDoc}
     */
    public function getMethodAnnotation(
        \ReflectionMethod $method,
        string $annotationName = null
    ) {
        $annotations = $this->getMethodAnnotations(
            $method->getDeclaringClass()->getName(),
            $method->name
        );

        if (!empty($annotations)) {
            foreach ($annotations as $annotation) {
                if (\class_exists($annotationName)) {
                    if ($annotation instanceof $annotationName) {
                        return $annotation;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Retrieve all annotations from a given constant of a class
     *
     * @param  string|object $class    Fully qualified name or instance of the class
     * @param  string        $constant Name of the constant
     * @return \Xeriab\Annotations\AnnotationsBag|array|mixed Annotations collection
     */
    public function getConstantAnnotations(
        $class = null,
        string $constant = null
    ) {
        return $this->getAnnotations(
            // new ReflectionConstant($class, $constant)
            new \ReflectionClassConstant($class, $constant)
        );
    }

    public function getConstantAnnotation(
        \ReflectionClassConstant $constant,
        string $annotationName = null
    ) {
        $annotations = $this->getConstantAnnotations(
            $constant->getDeclaringClass()->getName(),
            $constant->name
        );

        if (!empty($annotations)) {
            foreach ($annotations as $annotation) {
                if (\class_exists($annotationName)) {
                    if ($annotation instanceof $annotationName) {
                        return $annotation;
                    }
                }
            }
        }

        return null;
    }

    /**
     * Retrieve all annotations from a given function or closure
     *
     * @param  mixed $function Full qualified function name or closure
     * @return \Xeriab\Annotations\AnnotationsBag|array|mixed Annotations collection
     * @throws \ReflectionException                           If function is not found
     */
    public function getFunctionAnnotations($function = null)
    {
        return $this->getAnnotations(new \ReflectionFunction($function));
    }

    /**
     * Retrieve annotations from docblock of a given reflector
     *
     * @param \Reflector $reflection Reflector object
     * @param bool       $asBag      Return result as AnnotationsBag if true, array instead.
     *
     * @return \Xeriab\Annotations\AnnotationsBag|array|mixed Annotations collection
     */
    public function getAnnotations(\Reflector $reflection = null, bool $asBag = true)
    {
        $result = null;

        $ref = \method_exists($reflection, 'getDeclaringClass') ?
            $reflection->getDeclaringClass() : $reflection;

        $target = \get_class($reflection);

        if (Str::contains($target, '\\')) {
            $target = \explode('\\', $target);
            $target = \end($target);
        }

        $targetType = \strtolower(\substr($target, 10));

        $this->parser->setNamedResult($this->namedResult);

        switch ($targetType) {
            case 'property':
                $context = 'property ' . $ref->getName() . "::\$" . $reflection->getName();
                $this->parser->setTarget(Target::TARGET_PROPERTY);
                $this->parser->setImports($this->getPropertyImports($reflection));
                break;
            case 'class':
                $context = 'class ' . $ref->getName();
                $this->parser->setTarget(Target::TARGET_CLASS);
                $this->parser->setImports($this->getClassImports($ref));
                break;
            case 'method':
                $context = 'method ' . $ref->getName() . '::' . $reflection->getName() . '()';
                $this->parser->setTarget(Target::TARGET_METHOD);
                $this->parser->setImports($this->getMethodImports($reflection));
                break;
            case 'classconstant':
                $context = 'constant ' . $ref->getName() . '::' . $reflection->getName();
                $this->parser->setTarget(Target::TARGET_CONSTANT);
                $this->parser->setImports($this->getConstantImports($reflection));
                break;
            case 'constant':
                $context = 'constant ' . $ref->getName() . '::' . $reflection->getName();
                $this->parser->setTarget(Target::TARGET_CONSTANT);
                $this->parser->setImports($this->getConstantImports($ref));
                break;
            case 'function':
                $context = 'function ' . $reflection->getName();
                $this->parser->setTarget(Target::TARGET_FUNCTION);
                $this->parser->setImports($this->getFunctionImports($ref));
                break;
        }

        $this->parser->setIgnoredAnnotationNames(
            $this->getIgnoredAnnotationNames($ref)
        );

        $this->parser->setIgnoredAnnotationNamespaces(
            static::$globalIgnoredNamespaces
        );

        $doc = $reflection->getDocComment();

        if ($this->cache) {
            $key = $this->cache->getKey($doc);
            $result = $this->cache->get($key);

            if (!$result) {
                $result = $this->parser->parse($doc, $context);
                $this->cache->set($key, $result);
            }
        } else {
            $result = $this->parser->parse($doc, $context);
        }

        if ($this->namedResult) {
            if (!Arr::isAssociative($result)) {
                $rv = [];

                foreach ($result as $value) {
                    if (\is_object($value)) {
                        $item = \get_class($value);

                        if (Str::contains($item, '\\')) {
                            $v = \explode('\\', $item);
                            $v = \end($v);
                            // $rv[\lcfirst($v)] = $value;
                            $rv[($v)] = $value;
                        }
                    } elseif (\is_array($value) && \count($value) === 1) {
                        $rv[\array_keys($value)[0]] = \array_values($value)[0];
                        // D($value);
                    }
                }

                $result = $rv;
            }
        }

        foreach (static::$globalIgnoredNames as $key => $value) {
            if (isset($result[$key]) && $value === true) {
                unset($result[$key]);
            } else {
                foreach ($result as &$item) {
                    if ($item instanceof AnnotationsBag || \is_array($item)) {
                        // $result[\array_keys($item)[0]] = \array_values($item)[0];

                        if (isset($item[$key]) && $value === true) {
                            Arr::remove($result, $item);
                        }
                    }
                }
            }
        }

        return ($result) ? (($asBag) ? new AnnotationsBag($result) : $result) : null;
    }

    /**
     * Shortcut to create an instance of the default annotations Reader
     * bundled with the default Parser implementation
     *
     * @return \Xeriab\Annotations\Interfaces\ReaderInterface
     */
    public static function createFromDefaults(): ReaderInterface
    {
        return new static(new DocParser(), new ArrayCache);
    }
}
